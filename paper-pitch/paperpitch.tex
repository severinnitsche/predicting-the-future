\documentclass[t]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage{tikzpagenodes}
\usepackage{ifthen}
\usepackage{lmodern}
\usepackage{fontspec}

\usepackage[style=numeric]{biblatex}
\addbibresource{../references.bib}
\setlength{\skip\footins}{0cm}

\setsansfont{Karla} %% https://fonts.google.com/specimen/Karla
\setmonofont{Fira Mono} %% https://fonts.google.com/specimen/Fira+Mono

\usefonttheme[onlymath]{serif}
\boldmath{}

%% German style date formatting
\usepackage[ddmmyyyy]{datetime}
\renewcommand{\dateseparator}{.}

\usepackage[compatibility=false]{caption}
\captionsetup{singlelinecheck=off,justification=raggedleft,labelformat=empty,labelsep=none}

\usetheme{polynom}
\usecolortheme{polynomsecondgrade}
\usepackage{tikz}
\usetikzlibrary{graphs}

\title{Mining Association Rules}
\subtitle{Paper\\Pitch}
\date[Polynom]{\today}
\author{Severin Leonard Christian Nitsche}
\institute[Proseminar]{Proseminar Predicting the Future}

\begin{document}

\setPaletteGreen

\setbeamertemplate{title page}[polynom]{}

\begin{frame}[plain]
  \titlepage{}
\end{frame}

\nextsectionimage{figures/small/motivation.jpg}
\section{What are Association Rules?}

\begin{frame}
  \frametitle{Example} %% Add catch Line ie. small business
  \framesubtitle{What are Association Rules?}
  \begin{tikzpicture}[overlay, remember picture]
    \node (main)[
    yshift=-30mm,
    xshift=70mm
    ] {\includegraphics[width=0.6\columnwidth]{figures/small/amazon.png}\label{fig:esp32}};
    \node (secondary) at (main.north east)[
    yshift=-10mm,
    xshift=20mm
    ] {\includegraphics<2->[width=0.8\columnwidth]{figures/small/cross-marketing-1.png}\label{fig:cross-marketing-1}};
    \node (tertiary) at (secondary.south)[
    yshift=-20mm,
    xshift=10mm
    ] {\includegraphics<3->[width=0.8\columnwidth]{figures/small/cross-marketing-2.png}\label{fig:cross-marketing-2}};
    \node at (main.south)[yshift=-5mm] {\fontsize{16pt}{24pt}\selectfont\centering ESP32 on Amazon};
    \node at (tertiary.south)[
    yshift=20mm,
    xshift=-5mm
    ] {\includegraphics<4>[width=0.8\columnwidth]{figures/small/cross-marketing-3.png}\label{fig:cross-marketing-3}};
  \end{tikzpicture}
  %\caption{\fontsize{16pt}{24pt}\selectfont\centering Cross Marketing on Amazon}
\end{frame}

\begin{frame}
  \frametitle{Formal}
  \framesubtitle{What are Association Rules?}
  \pause
  \begin{itemize}
    \item<+-> Given a Database $\mathcal{D}$ \hspace{\fill} {\color{gray} $\mathcal{D}$ being a set of Transactions}
    \item<+-> A transaction $\mathit{T} \subseteq \mathcal{I}$ is a set of Items \hspace{\fill} {\color{gray} $\mathcal{I}$ being all (relevant) Items}
    \item<+-> And a unique $\mathit{TID}$ associated with each Transaction $\mathit{T}$
  \end{itemize}
  \vspace{\fill}
  \uncover<+->{An \textit{association rule} takes the form $X \Rightarrow Y$ where $X,Y \subseteq \mathcal{I}, X \cap Y = \emptyset$\\}
  \vspace{\fill}
  \usebeamerfont{footline}Source: \fullcite{apriori}
\end{frame}

\begin{frame}
  \frametitle{Formal - Metrics}
  \framesubtitle{What are Association Rules?}
  \pause
  \begin{itemize}
    \item<+-> \textit{confidence} $c$ \hspace{\fill} {\color{gray} $\mathit{T} \in \mathcal{D}: X \subset \mathit{T} \Rightarrow Y \subset \mathit{T}$ in $c\%$ of the cases}
    \item<+-> \textit{support} $s$ \hspace{\fill} {\color{gray} $\mathit{T} \in \mathcal{D}: X \cup Y \subseteq \mathit{T}$ in $s\%$ of the cases}
    \item<+-> {\color{gray} \textit{lift}}
    \item<+-> {\color{gray} \textit{conviction}}
  \end{itemize}
  \vspace{\fill}
  \usebeamerfont{footline}Source: \fullcite{apriori}
\end{frame}

\nextsectionimage{figures/small/boredom.jpg}
\section{Applications}

\begin{frame}
  \frametitle{Market Basket Analysis}
  \framesubtitle{Applications}
  \pause
  \begin{itemize}
    \item<+-> Customer Manipulation
    \begin{itemize}
      \item<+-> Cross Selling
      \item<+-> Product Placement
      \item<+-> Affinity Promotion
    \end{itemize}
    \item<+-> Fraud Detection
    \item<+-> Demographic Relations
  \end{itemize}
\end{frame}

\section{Mining Association Rules}

\setbeamertemplate{banner page}[polynom][Apriori]
\begin{frame}<handout:0>
  \usebeamertemplate{banner page}
\end{frame}

\setbeamertemplate{banner page}[polynom][Rule Induction]
\begin{frame}<handout:0>
  \usebeamertemplate{banner page}
\end{frame}

\setbeamertemplate{banner page invert}[polynom][FP-Growth]
\begin{frame}<handout:0>
  \usebeamertemplate{banner page invert}
\end{frame}

\begin{frame}
  \frametitle{Methods}
  \framesubtitle{Mining Association Rules}
  \begin{itemize}
    \item {\color{gray} Apriori \hspace{\fill} $[14]$}
    \item {\color{gray} Rule Induction \hspace{\fill} $[11]$}
    \item[$\rightarrow$] FP-Growth
    \pause
    \begin{itemize}
      \item<+-> Data Structure \hspace{\fill} Frequent Pattern Tree
      \item<+-> Algorithms \hspace{\fill} FP-tree construction\\
      \hspace{\fill} FP-growth
      \item<+-> Proofs
    \end{itemize}
  \end{itemize}
  \vspace{\fill}
  \usebeamerfont{footline}Reference: \fullcite{fp-growth}
\end{frame}

\begin{frame}<handout:0>
  \frametitle{Role Models}
  \framesubtitle{Mining Association Rules}
  \begin{figure}
    \centering
    \includegraphics[width=0.7\columnwidth]{figures/small/Leif-Kobbelt.png}\label{fig:Leif-Kobbelt}
    \caption{\fontsize{16pt}{24pt}\selectfont\centering Some random Professor}
  \end{figure}
\end{frame}

\nextsectionimage{figures/small/outlook.png}
\section{Further Applications} %% TODO: change section title

\begin{frame}
  \frametitle{Outlook}
  \framesubtitle{Further Applications}
  \pause
  \begin{itemize}
    \item<+-> Document Clustering
    \item<+-> Real Time Mining
    \item<+-> Stream Mining \hspace{9mm} IOT
  \end{itemize}
  \vspace{\fill}
  \usebeamerfont{footline}References: \fullcite{document-clustering}
  \\\fullcite{cps}
\end{frame}

\setbeamertemplate{banner page invert}[polynom][Thank You!]
\begin{frame}
  \usebeamertemplate{banner page invert}
\end{frame}

\end{document}

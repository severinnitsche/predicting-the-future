# Predicting the Future

The proseminar focuses on using various data mining techniques to predict and optimize processes yet to happen. The focus resides on:

- exploring a topic outside the regular curriculum
- familiarizing one with scientific work
- Presentation of research

## Assignments

Out of the topics:
- [ ] [Classificarion Trees](https://www.youtube.com/watch?v=_L39rN6gz7Y)
- [ ] [Naive Bayesian](https://www.youtube.com/watch?v=O2L2Uv9pdDA)
- [ ] [Artificial Neural Networks](https://www.youtube.com/watch?v=CqOfi41LfDw)
- [ ] [Support Vector Machines](https://www.youtube.com/watch?v=efR1C6CvhmE)
- [ ] [DBSCAN Clustering](https://www.youtube.com/watch?v=RDZUdRSDOok)
- [ ] K-Nearest Neighbours
- [ ] Linear Regression
- [ ] K-Means Clustering
- [ ] EM-Clustering
- [ ] Self-organizing maps
- [ ] Rule Induction
- [ ] Ensemble Learners
- [x] **Mining Association Rules**
- [ ] Apriori Algorithm for Association Analysis

I got assigned to *Mining Association Rules*

## Sources

Vijay Kotu and Bala Deshpande, PhD. Predictive Analytics and Data Mining

 Trevor Hastie, Robert Tibshirani, Jerome Friedman (2017). The Elements of Statistical Learning: Data Mining, Inference and Prediction

## On LaTeX

We use `lualatex` around here (otherwise [polynom](https://git.rwth-aachen.de/ACHinrichs/LaTeX-templates) gets cancelled)

```shell
lualatex -output-directory=./build paperpitch.tex
lualatex -output-directory=./build paperpitch.tex
```

In fact that was a lie. We use `pdflatex` in conjunction with `bibtex` because [reasons](http://www.ctan.org/pkg/ieeetran) (Thanks [ComSys](https://www.comsys.rwth-aachen.de/)).

```shell
pdflatex -output-directory=./build paper.tex
cd build
bibtex paper
cd ..
pdflatex -output-directory=./build paper.tex
pdflatex -output-directory=./build paper.tex
```

*make sure that `build` exists prior to executing this*

*build twice because tikz is weird*

## Paper Pitch
[how 2](https://www.indeed.com/career-advice/career-development/how-to-write-a-pitch)

## Sketch

### What am I trying to ~achieve~ portray?

> das Thema [Mining Association Rules] in einem Vortrag [..] **verständlich darzustellen**

**Paper Pitch**
> Ziel ist es, Ihren Vortrag **zu bewerben** und Interesse [..] zu wecken

#### What are Association Rules?
- If X then Y
- (Antecedent; Consequent)

#### What are they used for? (Applications)
- Market Basket Analysis (BOORING!)
- People and Demographics

#### Mining Association Rules
- Apriori
- (Rule Induction)
- FP-Growth

#### Further Applications
1. Document Clustering
2. Real Time Mining
 1. >sometimes the data is generated so fast it should not be stored at all, but analysed directly at the source and the findings stored instead
 2. Stream Mining; IOT

#### Extra: Implementation in Python and SQLite

### What are the steps needed for this?
1. Sketch content of sections above
2. refine catch lines
3. search fun images (step a li'l bit down)
4. Rehearse and get Feedback

### What is really needed and what is out of scope?
- Python implementation is pb not realistic **but fun**

## Deepnote

[Deepnote](https://deepnote.com/workspace/severin-nitsche-3b49-725c9623-15be-48a6-8fe1-e5868d98eed9/project/Predicting-The-Future-WZ1uro3ITSOWOwb6up1beA/%2Fpredicting-the-future%2FREADME.md)
